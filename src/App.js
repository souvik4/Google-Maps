import {
  Box,
  Button,
  ButtonGroup,
  Flex,
  HStack,
  IconButton,
  Input,
  SkeletonText,
  Text,
} from '@chakra-ui/react'
import { FaLocationArrow, FaTimes } from 'react-icons/fa'
import {useJsApiLoader, GoogleMap, Marker, Autocomplete} from '@react-google-maps/api';
import {useState} from 'react'

const center = {lat:22.6096, lng: 88.3179}

function App() {

  const {isLoaded} = useJsApiLoader({
    googleMapsApiKey: process.env.Api_Key,
    libraries: ['places'] 
  })

  const [map, setMap] = useState( /**@type google.maps.Map */ (null));

  if(!isLoaded){
   return <SkeletonText />
  }

  
  
  return (
    <Flex
      position='relative'
      flexDirection='column'
      alignItems='center'
      background-color='white'
      h='100vh'
      w='100vw'
    >
      <Box position='absolute' left={0} top={0} h='100%' w='100%'>
        <GoogleMap 
          center={center} 
          zoom={15} 
          mapContainerStyle={{width: '100%', height: '100%'}}
          options={{
            zoomControl: false,
            fullscreenControl: false,
            mapTypeControl: false
          }}
          onLoad={(map) => setMap(map)}
        >
        
        <Marker position={center}/>
        </GoogleMap>
      </Box>

      <Box
        p={4}
        borderRadius='lg'
        mt={4}
        bgColor='white'
        shadow='base'
        minW='container.md'
        zIndex='modal'
      >
        <HStack spacing={4}>
          <Autocomplete>
          <Input type='text' placeholder='Origin' />
          </Autocomplete>

          <Autocomplete>
          <Input type='text' placeholder='Destination' />
          </Autocomplete>

          <ButtonGroup>
            <Button colorScheme='pink' type='submit'>
              Calculate Route
            </Button>
            <IconButton
              aria-label='center back'
              icon={<FaTimes />}
              onClick={() => alert("GoodBye")}
            />
          </ButtonGroup>
        </HStack>
        <HStack spacing={4} mt={4} justifyContent='space-between'>
          <Text>Distance: </Text>
          <Text>Duration: </Text>
          <IconButton
            aria-label='center back'
            icon={<FaLocationArrow />}
            isRound
            onClick={() => map.panTo(center)}
          />
        </HStack>
      </Box>
    </Flex>
  )
}

export default App